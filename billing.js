var doc = new jsPDF();
var specialElementHandlers = {
    '.container': function (element, renderer) {
        return true;
    }
};

$('#chargebtn').click(function () {   
    doc.fromHTML($('#main-billing').html(), 50, 10,  {
        'width': 170,
            'elementHandlers': specialElementHandlers
    });
    doc.save('BILL.pdf');
});

