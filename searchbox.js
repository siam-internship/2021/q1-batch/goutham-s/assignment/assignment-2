$(function() {
	var menuitems  =  [
	   "Cake",
	   "Cheesecake",
	   "Crossiant",
	   "Capuccino",
     "Cookies",
     "Green Apple",
     "Strawberry Dessert",
     "Chocolate Eclair Dessert",
     "Chocolate Truffle Dessert",
     "Fancy Cake",
     "Strawberry Lady Finger",
     "Fruit Salad",
     "Icecream Sandwich",
     "Cake with Strawberry",
     "Strawberry Dessert",
     "Latte",
     "Sweets",
     "Juice",
     "Discount",
     "Fruit Yoghurt"
	];
	$( "#items" ).autocomplete({
	   source: menuitems });
 });